from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .forms import *
from .models import *

response = {}

def index(request):
	return render(request, 'landingPage.html', {})

def json(request, category=''):
    apis = requests.get(url="https://www.googleapis.com/books/v1/volumes?q="+{category})
    data = apis.json()
    return JsonResponse(data)


def daftar_buku(request):
    return render(request, 'landingPage.html')

def formRegister(request):
    response = {'form_daftar' : FormRegister}
    return render(request, 'formRegister.html', response)

@csrf_exempt
def check_email(request):
    email = request.POST['email']
    db = FormRegisterModels.objects.all()
    terdaftar = 'Email sudah pernah terdaftar'
    for data in db:
        if email == data.email:
            return JsonResponse({'message': terdaftar})
    return JsonResponse({'message': 'Ok'})

@csrf_exempt
def subscribe(request):
    nama = request.POST['nama']
    email = request.POST['email']
    password = request.POST['password']
    FormRegisterModels(nama=nama, email=email, password=password).save()
    return JsonResponse({'message': 'Anda telah terdaftar'})



	
	




# Create your views here.

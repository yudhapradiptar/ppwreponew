from django.urls import re_path, path
from django.conf.urls import url
from .views import *

app_name = 'lab_9'

urlpatterns = [
    path('landingPage', index, name='index'),
    path('json', json, name='json'),
    path('formRegister/', formRegister, name='formRegister'),
    path('email-check/', check_email, name='check_email'),
    path('subscribe/', subscribe, name='subscribe'),


]

from django import forms
from .models import FormRegisterModels
class FormRegister(forms.Form):
    attrs = {
        'class': 'form-control'
    }

    nama = forms.CharField(label = 'Nama Lengkap', required=True, widget=forms.TextInput(attrs=attrs), max_length=30)
    email = forms.EmailField(label = 'Email', required=True, widget=forms.EmailInput(attrs=attrs), max_length=30)
    password = forms.CharField(label = 'Password', required=True, widget=forms.PasswordInput(attrs=attrs), max_length=15)
    class Meta:
    	model = FormRegisterModels
    

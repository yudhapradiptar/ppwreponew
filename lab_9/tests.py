from django.test import TestCase
from django.test import Client
from django.http import HttpRequest
from django.urls import resolve
from .views import *
# Create your tests here.

class PageTestCase(TestCase):
	def test_url_lab6(self):
		response = self.client.get('/')
		self.assertEqual(response.status_code, 200)

	def test_welcome_to_my_library(self):
		request = HttpRequest()
		response = self.client.get('/')
		self.assertContains(response, 'Welcome to my Library')

	def test_name_is_there(self):
		request = HttpRequest()
		response = self.client.get('/')
		self.assertContains(response, 'Yudha Pradipta Ramadan')

	def test_npm_is_there(self):
		request = HttpRequest()
		response = self.client.get('/')
		self.assertContains(response, '1706043424')

	def test_favCount_is_there(self):
		request = HttpRequest()
		response = self.client.get('/')
		self.assertContains(response, 'Favorite Books : ')




